/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */

import org.springframework.context.support.ClassPathXmlApplicationContext;

import client.IClient;

public class SpringMagasinApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext c = new ClassPathXmlApplicationContext("client-beans.xml",
				SpringMagasinApplication.class);
		IClient client = (IClient) c.getBean("client");
		client.run();
	}

}
