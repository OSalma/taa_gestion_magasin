import org.springframework.context.support.ClassPathXmlApplicationContext;

import client.IClient;

/**
 * 
 */

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public class SpringMagasinApplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("client-beans.xml",
				SpringMagasinApplication.class);
		IClient client = (IClient) context.getBean("client");
		client.run();

	}

}
