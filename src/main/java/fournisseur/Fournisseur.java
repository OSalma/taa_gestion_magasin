/**
 * 
 */
package fournisseur;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public class Fournisseur implements IFournisseur{

	@Override
	public int getPrice(String referenceArticle) {
		return Math.abs(referenceArticle.hashCode() % 500);
	} 

	@Override
	public void order(String referenceArticle, int quantite) {
		System.out.println(quantite + " × " + referenceArticle + " = " + (quantite*this.getPrice(referenceArticle)) + "€");
	}

}
