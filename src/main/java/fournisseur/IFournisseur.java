/**
 * 
 */
package fournisseur;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public interface IFournisseur {

	public int getPrice(String referenceArticle);
	
	public void order(String referenceArticle, int quantite);
}
