/**
 * 
 */
package aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
@Aspect
public class LogAspect {
	@Before("execution(public * *(..))")
	public void log(JoinPoint pjp) {
		System.out.println("log");
	}
}
