/**
 * 
 */
package magasin;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public interface ILane {
	
	public void addItemToCart(String article);
	
	public void pay(String panier);

}
