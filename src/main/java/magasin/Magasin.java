/**
 * 
 */
package magasin;

import fournisseur.IFournisseur;
import banque.IBanque;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public class Magasin implements IMagasin{
	
	private IBanque banque;
	private IFournisseur fournisseur;

	public IBanque getBanque() {
		return banque;
	}

	public void setBanque(IBanque banque) {
		this.banque = banque;
	}

	public IFournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(IFournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	@Override
	public int getPrice(String article) {
		
		return fournisseur.getPrice(article);
		
	}

	@Override
	public boolean isAvailable(String article) {
		
		return true;
	}

	@Override
	public void oneShotOrder(String article) {
		banque.transfert("client", "magasin", this.fournisseur.getPrice(article));
		this.fournisseur.order(article, 1);
		
	}

	@Override
	public void addItemToCart(String article) {
		System.out.println("L'article " + article + " est ajouté à la carte.");
	}

	@Override
	public void pay(String panier) {
		
		banque.transfert("client", "store", fournisseur.getPrice(panier));
		System.out.println("Le panier " + panier + " est payé.");
		
	}

}
