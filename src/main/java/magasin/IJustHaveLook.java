/**
 * 
 */
package magasin;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public interface IJustHaveLook {
	
	public int getPrice(String article);
	
	public boolean isAvailable(String article);

}
