/**
 * 
 */
package banque;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public interface IBanque {
	
	public void transfert(String vendeurCompteBancaire, String acheteurCompteBancaire, int montant);

}
