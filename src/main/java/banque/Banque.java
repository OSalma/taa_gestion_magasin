/**
 * 
 */
package banque;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public class Banque implements IBanque {

	@Override
	public void transfert(String vendeurCompteBancaire,
			String acheteurCompteBancaire, int montant) {
		
		System.out.println(vendeurCompteBancaire + " >> " + montant + "€ >> " + acheteurCompteBancaire);
		
	}

}
