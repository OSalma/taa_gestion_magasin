/**
 * 
 */
package client;

import magasin.*;

/**
 * @author Ouhammouch Salma M2 MITIC
 *
 */
public class Client implements IClient{
	
	private IJustHaveLook justHaveLook;
	private IFastLane fastLane;
	private ILane lane;	
	
	public IJustHaveLook getJustHaveALook() {
		return justHaveLook;
	}

	public void setJustHaveALook(IJustHaveLook justHaveALook) {
		this.justHaveLook = justHaveALook;
	}

	public IFastLane getFastLane() {
		return fastLane;
	}

	public void setFastLane(IFastLane fastLane) {
		this.fastLane = fastLane;
	}

	public ILane getLane() {
		return lane;
	}

	public void setLane(ILane lane) {
		this.lane = lane;
	}

	@Override
	public void run() {
		System.out.println("Le prix de l'article 1 est " +justHaveLook.getPrice("article1"));
		System.out.println("Article 1 acheté");
		this.fastLane.oneShotOrder("article1");
		
		System.out.println("Le prix de l'article 2 est  " +justHaveLook.getPrice("article2"));
		this.lane.addItemToCart("article2");
		this.lane.pay("article2");
	}

}
